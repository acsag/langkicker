from django.conf.urls import patterns, include, url

#from django.contrib import admin
#admin.autodiscover()



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'langkicker.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'machina.views.word_list_view', name="word list"),
	url(r'^word-add$', 'machina.views.word_add_view', name="word add"),

)
