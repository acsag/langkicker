from django.forms import ModelForm, RadioSelect, Select
from django.utils.translation import ugettext_lazy
import re

from .models import Word, Translation, LANG_CHOICES, FREQ_CHOICES

class WordForm(ModelForm):
	class Meta:
		model = Word
		#fields = ['name',
		labels = {
			'freq': ugettext_lazy('Frequency of recurrence'),
		}
		widgets = {
			'freq': Select( choices=FREQ_CHOICES ),
		}
		"""
		error_messages = {
			'freq': {
				'max_length': 
		"""





