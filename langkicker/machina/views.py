from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import RequestContext, loader


from .models import Word, Translation
from .forms import WordForm

def helloworld(request):
	return HttpResponse("<h1>niiice</h1>")


def word_list_view(request):
	
	_dict = {}
	wl = Word.objects.all()
	tl = Translation.objects.all()
	_dict['wordz'] = wl
	
	template = loader.get_template('word_list.html')
	context = RequestContext( request, _dict )
	return HttpResponse( template.render(context) )


def word_add_view(request):		## TODO: check if duplicate
	
	if request.method == 'POST':
		form = WordForm(request.POST)
		if form.is_valid():
			form.save(commit=True)
			return redirect('/')
	
	else:
		form = WordForm()
	
	return render(request, 'word_add.html', {'word_form': form})



def word_update_view(request):
	pass
	#if request.method == 'POST':


