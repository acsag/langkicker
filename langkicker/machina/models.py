from django.db import models



LANG_CHOICES = (
		('EN', 'English'),
		('TR', 'Turkce'),
		('OT', 'Other'),
	)
FREQ_CHOICES = ((1,1),(2,2),(3,3),(4,4),(5,5))

class Word(models.Model):
	name = models.CharField(max_length=50)
	lang = models.CharField(max_length=2, choices=LANG_CHOICES)
	date = models.DateField(auto_now_add=True, choices=FREQ_CHOICES) #date_added
	freq = models.IntegerField(blank=True, default=3)	# apperance frequency bw [1..5]

	def __unicode__(self):
		return self.name



class Translation(models.Model):
	word_from = models.ForeignKey(Word, related_name="translatee")
	hint = models.TextField(blank=True)
	word_to = models.ManyToManyField(Word, related_name="translated")#, null=True, blank=True)
	description = models.TextField(blank=True)
	date = models.DateField(auto_now_add=True) #translation date
	
	def __unicode__(self):
		if self.word_from : fr=self.word_from.name
		else: fr="[nothing]"
		
		to="| "
		if self.word_to.exists():
			for i in self.word_to.get_queryset():
				to=to + i.name + ", "
			to=to+"|"

		else:
			to="[nothing]"
		
		return fr+" => "+to


