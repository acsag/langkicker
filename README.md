## This is a repo of a Django app to exercise vocabulary.

App gives a list of words and each word has hint, a translation word (up to two words), a longer description, and other material (for picture, video and audio [LATER])


Software used in the project:
* git
* virtualenv
* Python 2.7
* Django 1.6.6
* South (Django plugin)
* a js/css lib if necessary [LATER]
 

Repo owner: Ali Can SAG